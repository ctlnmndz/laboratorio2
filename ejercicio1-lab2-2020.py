#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Libreria para calcular la moda.
from statistics import mode


def leer_archivo():
    try:
        # Se lee el archivo.
        with open('top50.csv', 'r') as archivo:
            lineas = archivo.readlines()
        archivo.close()

    except IOError:
        lineas = -1

    # Contenido del archivo.
    return lineas


# Función para sacar la cantidad y moda de los artistas.
def artistas(artista1):

    # Se elimina el primer elemento de la lista (nombre de las variables).
    del artista1[0]

    largo = len(artista1)
    print("La cantidad de artistas es: ", largo)

    # Se aplana la lista.
    lista_nueva = [item for lista in artista1 for item in lista]

    # Se saca la moda.
    print(" El artista conmás canciones en el ranking es:", mode(lista_nueva))


# Función para sacar la canción mas bailable y menos bailable.
def bailables(bailar,generos,canciones, tiempos):

    # Se elimina el primer indice (genero, danceability).
    del bailar[0]
    del generos[0]
    del canciones[0]
    del tiempos[0]

    # se aplanan las listas.
    lista_nueva_bailable = [item for lista in generos for item in lista]
    lista_nueva = [item for lista in bailar for item in lista]
    lista_nombre_canciones = [item for lista in canciones for item in lista]
    lista_tiempo_minuto = [item for lista in tiempos for item in lista]

    # Se sacan los indices de las musicas mas bailables.
    indice1 = lista_nueva.index('90')
    indice2 = lista_nueva.index('88')

    # Se elimina el indice para que no se repita al buscar los 3 más bailables.
    del lista_nueva[indice2]
    indice3 = lista_nueva.index('88')
    indice4 = lista_tiempo_minuto.index('85')

    # Se elimina el indice para que no se repita al buscar las 3 más lentas.
    del lista_tiempo_minuto[indice4]
    indice5 = lista_tiempo_minuto.index('85')
    indice6 = lista_tiempo_minuto.index('88')

    # Se imprimen los generos mas bailables.
    print("Los tres géneros más bailables son: ", lista_nueva_bailable[indice1],
                                                lista_nueva_bailable[indice2],
                                                lista_nueva_bailable[indice3+1])
    # Se imprimen las canciones más lentas.
    print("Las 3 canciones más lentas son: ", lista_nombre_canciones[indice4],
                                              lista_nombre_canciones[indice5+1],
                                              lista_nombre_canciones[indice6+1])



# Función para comparar si la canción más popular es la menos ruidosa.
def cancion_ruidosa(ruidosa, canciones, popularidad, artista1):

    # Se elimina el primer indice (popularity, artists.Name).
    del ruidosa[0]
    del canciones[0]
    del popularidad[0]
    del artista1[0]

    # se aplanan las listas.
    lista_ruidosa = [item for lista in ruidosa for item in lista]
    aux_ruidosa = [item for lista in ruidosa for item in lista]
    lista_nombre_canciones = [item for lista in canciones for item in lista]
    lista_popular = [item for lista in popularidad for item in lista]
    lista_artistas = [item for lista in artista1 for item in lista]

    # Se sacan los indices de las canciones menos ruidosas.
    indice1 = lista_ruidosa.index('-11')
    del lista_ruidosa[indice1]
    indice2 = lista_ruidosa.index('-11')
    del lista_ruidosa[indice2]
    indice3 = lista_ruidosa.index('-11')

    # Se saca el indice de la canción más popular.
    indice4 = lista_popular.index('95')
    cancion_popular = lista_nombre_canciones[indice4]

    # Se en listan las canciones menos ruidosas ara compararlas.
    lista_menos_ruidosa = []
    lista_menos_ruidosa.append(lista_nombre_canciones[indice1])
    lista_menos_ruidosa.append(lista_nombre_canciones[indice2+1])
    lista_menos_ruidosa.append(lista_nombre_canciones[indice3+2])

    # Si la canción más popular es la menos ruidosa se en listan lo indicado.
    if cancion_popular in lista_menos_ruidosa:
        lista = []
        lista.append(lista_artistas[indice4])
        lista.append(lista_nombre_canciones[indice4])
        lista.append(lista_popular[indice4])
        lista.append(aux_ruidosa[indice4])
        print(lista)
    # Sino se imprime que no.
    else:
        print("La canción popular no es la menos ruidosa")


# Función para calcular la mediana.
def mediana(ruidosa):

    del ruidosa[0]

    # Se aplana la lista.
    lista_ruidosa = [item for lista in ruidosa for item in lista]

    # Se ordena la lista de menor a mayor.
    lista_ruidosa.sort()

    # Se saca los valores del medio.
    indice1 = len(lista_ruidosa)/2
    indice2 = indice1 + 1

    # Se calcula la mediana.
    mediana = (indice1 + indice2)/2
    print("La mediana es: ", mediana)


# Función donde se procesan datos.
def procesar_datos(archivo):

    artista1 = []
    bailar = []
    generos = []
    tiempos = []
    canciones = []
    ruidosa = []
    popularidad = []

    for linea in archivo:
        # Filtra las columnas separadas por coma.
        aux = linea.split(',')
        posicion = aux[0:1]
        nombre = aux[1:2]
        # Se listan los nombres de las canciones.
        canciones.append(nombre)
        artista = aux[2:3]
        # Se listan los artistas.
        artista1.append(artista)
        genero = aux[3:4]
        # Se listan los generos.
        generos.append(genero)
        tiempo = aux[4:5]
        # Se listan los beasts.
        tiempos.append(tiempo)
        energia = aux[5:6]
        bailable = aux[6:7]
        # Se listan los valores de danceability.
        bailar.append(bailable)
        ruido = aux[7:8]
        # Se listan los valores de ruido.
        ruidosa.append(ruido)
        popular = aux[13:14]
        # Se listan los valores de popularidad.
        popularidad.append(popular)


    # Se llaman funciones.
    mediana(ruidosa)
    artistas(artista1)
    bailables(bailar, generos, canciones, tiempos)
    cancion_ruidosa(ruidosa, canciones, popularidad, artista1)



# Main.
if __name__ == "__main__":
    archivo = leer_archivo()
    procesar_datos(archivo)
